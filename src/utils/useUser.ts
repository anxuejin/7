import { reactive, onMounted } from "vue"
const useUser = () => {
  const user = reactive({
    name: "",
    email: "",
    islogin: true,
  })
  try {
    const userInfo = JSON.parse(window.localStorage.getItem("user") || "")
    // console.log(userInfo);
    if (userInfo && userInfo.name) {
      user.name = userInfo.name
      user.email = userInfo.email
      user.islogin = true
    } else {
      user.islogin = false
    }
  } catch (error) {
    console.error(error)
  }
  return user
}
export default useUser
