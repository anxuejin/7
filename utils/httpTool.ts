import axios from "axios";
import { message } from "ant-design-vue";
import CODEMAP from "./codeMap";

export interface PageNationParans {
  page?: number;
  pageSize?: number;
}
export interface Params extends PageNationParans {
  [propertyName: string]: string | number | boolean | undefined;
}
console.log(process.env.VUE_APP_BASEURL)
const httpTool = axios.create({
  timeout: 10000,
  baseURL: process.env.VUE_APP_BASEURL,
});

httpTool.interceptors.request.use((config) => {
  // console.log(config);
  return config;
}, (error) => {
  return Promise.reject(error);
})

//响应拦截
httpTool.interceptors.response.use(
  (response) => {
    // console.log(response);
    if (response.data?.success) {
      return response.data;
    }
    message.error(
      response.data?.msg || CODEMAP[response.data?.statusCode || 500]
    );
    return response;
  },
  (error) => {
    //超时情况
    if (error.code === "ECONNABORTED") {
      message.error("您当前网络环境不好，请刷新重试");
    } else {
      message.error(
        error.response?.statusText ||
        CODEMAP[error.response.data?.status || 400]
      );
    }
    return Promise.reject(error);
  }
);

export default {
  ...httpTool,
  get(url: string, params: Params = {}) {
    return httpTool.get(url, {
      params,
    })
  },
  post(url: string, data = {}) {
    return httpTool.post(url, data)
  },
};
